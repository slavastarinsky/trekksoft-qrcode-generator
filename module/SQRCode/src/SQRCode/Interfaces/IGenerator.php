<?php

namespace SQRCode\Interfaces;

interface IGenerator
{
	public function getImageData();
	
	public function setText($text);
	
	public function setDimensions($with, $height);

    public function getImageType();
}