<?php

namespace SQRCode\Factory\Exceptions;

class NoGeneratorException extends \Exception
{
    public $message = "Please, set QR Code generator";
} 