<?php

namespace SQRCode\Factory;

use SQRCode\Factory\Exceptions\NoGeneratorException;
use SQRCode\Interfaces\IGenerator;

class QRCodeProducer
{
    /** @var IGenerator */
    private $generator = null;
    private $text;
    private $dimensions;

    public function __construct($text, $width, $height)
    {
        $this
            ->setText($text)
            ->setDimensions($width, $height)
        ;
    }

    /**
     * @param IGenerator $generator
     */
    public function setGenerator(IGenerator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @param $width
     * @param $height
     * @return $this
     */
    public function setDimensions($width, $height)
    {
        $this->dimensions = [
            (int) $width, (int) $height
        ];
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function render()
    {
        if (null == $this->generator) {
            throw new NoGeneratorException();
        }

        return $this->generator
            ->setText($this->text)
            ->setDimensions($this->dimensions[0], $this->dimensions[1])
            ->getImageData()
        ;
    }

    public function getImageType()
    {
        if (null == $this->generator) {
            throw new NoGeneratorException();
        }

        return $this->generator->getImageType();
    }

    public function getGeneratorClassName()
    {
        return get_class($this->generator);
    }
} 