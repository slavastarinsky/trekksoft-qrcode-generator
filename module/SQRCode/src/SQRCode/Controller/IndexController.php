<?php

namespace SQRCode\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use SQRCode\Factory\QRCodeProducer;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $generators = [
            new \SQRCode\Generator\GoogleQRCodeGenerator(),
            new \SQRCode\Generator\BaconQRCodeGenerator(),
        ];

        $qrCode = new QRCodeProducer('TrekkSoft', 250, 250);
        $qrCode->setGenerator(
            $generators[array_rand($generators)]
        );

        return new ViewModel([
            'qrCode' => $qrCode,
        ]);
    }
}
