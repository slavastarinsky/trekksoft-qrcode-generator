<?php

namespace SQRCode\Generator;

use SQRCode\Interfaces\IGenerator;
use BaconQrCode\Renderer\Image\Svg;
use BaconQrCode\Renderer\Image\Png;
use BaconQrCode\Writer;

class BaconQRCodeGenerator implements IGenerator
{
	private $generator;
    private $imageType;

    protected $text;

	public function __construct()
	{
        if (function_exists('imagepng')) {
            $this->generator = new Png();
            $this->imageType = 'png';
        } else {
            $this->generator = new Svg();
            $this->imageType = 'svg+xml';
        }
	}

    public function getImageData()
    {
        $writer = new Writer($this->generator);
        return $writer->writeString($this->text);
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function setDimensions($width, $height)
    {
        $this->generator->setWidth($width);
        $this->generator->setHeight($height);
        $this->generator->setMargin(0);
        return $this;
    }

    public function getImageType()
    {
        return $this->imageType;
    }
}