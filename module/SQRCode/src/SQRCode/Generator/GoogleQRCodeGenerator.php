<?php

namespace SQRCode\Generator;

use QRCode\Service\QRCode;
use SQRCode\Interfaces\IGenerator;

class GoogleQRCodeGenerator implements IGenerator
{
	private $generator;
    private $imageType = 'png';

	public function __construct()
	{
        $this->generator = new QRCode();
	}

    public function getImageData()
    {
        $this->generator->isHttps();
        return file_get_contents(
            $this->generator->getResult()
        );
    }

    public function setText($text)
    {
        $this->generator->setData($text);
        return $this;
    }

    public function setDimensions($width, $height)
    {
        $this->generator->setDimensions($width, $height);
        return $this;
    }

    public function getImageType()
    {
        return $this->imageType;
    }
}